# GD32_Nilai

Sandbox implementing GD32 support into [NilaiTFO](https://github.com/smartel99/NilaiTFO).

## Cloning

```
git clone https://gitlab.com/conception-electronique-prive/gd32/gd32_nilai --recursive
```

## In this repository
